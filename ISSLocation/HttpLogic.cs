﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ISSLocation
{
    class HttpLogic
    {
        public static async Task<string> GetData()
        {
            var client = new HttpClient();
            var api = "http://tle.ivanstanojevic.me";
            var endpoint = $"/api/tle/25544";
            try
            {
                var httpResponse = await client.GetAsync($"{api}{endpoint}");
                httpResponse.EnsureSuccessStatusCode();
                string responseBody = await httpResponse.Content.ReadAsStringAsync();
                return responseBody;
            }
            catch (Exception e)
            {
                
            }
            return "";
        }
    }
}
