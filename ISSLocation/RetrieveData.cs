﻿using Newtonsoft.Json.Linq;

namespace ISSLocation
{
    public static class RetrieveData
    {
        public static Satellite GetSatelliteObjectFromData()
        {
            var str = HttpLogic.GetData();
            var jsonData = JObject.Parse(str.Result);
            var MySatellite = Newtonsoft.Json.JsonConvert.DeserializeObject<Satellite>(str.Result);
            return MySatellite;
        }
    }
}
