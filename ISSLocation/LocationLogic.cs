﻿
namespace ISSLocation
{
    public class LocationLogic
    {
        public void GetLocation()
        {
            var Satellite = RetrieveData.GetSatelliteObjectFromData();
            var LocationSatellite = SatelliteLocation.GetSatelliteLocation(Satellite.LocationInfo);
        }
    }
}
