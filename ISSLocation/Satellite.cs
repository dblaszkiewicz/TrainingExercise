﻿using Newtonsoft.Json;

namespace ISSLocation
{
    public class Satellite
    {
        [JsonProperty("satelliteId")]
        public int SatelliteId {get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("line1")]
        public string BasicInfo { get; set; }

        [JsonProperty("line2")]
        public string LocationInfo { get; set; }
    }
}
