﻿using System;

namespace ISSLocation
{
    class SatelliteLocation
    {
        public int SatelliteNumber { get; set; }
        public double Inclination { get; set; }
        public double RightAscensionOfTheAscendingNode { get; set; }
        public string Eccentricity { get; set; }
        public double ArgumentOfPerigee { get; set; }
        public double MeanAnomaly { get; set; }
        public double MeanMotion { get; set; }
        public double RevolutionNumber { get; set; }
        public int Checksum { get; set; }

        public static SatelliteLocation GetSatelliteLocation(string str)
        {
            var SatLocation = new SatelliteLocation();
            var array = str.Split(" ");
            SatLocation.SatelliteNumber = Int32.Parse(array[0]);
            SatLocation.Inclination = Double.Parse(array[1]);
            SatLocation.RightAscensionOfTheAscendingNode = Double.Parse(array[2]);
            SatLocation.Eccentricity = array[3];
            SatLocation.ArgumentOfPerigee = Double.Parse(array[4]);
            SatLocation.MeanAnomaly = Double.Parse(array[5]);
            SatLocation.RevolutionNumber = Double.Parse(array[6]);
            SatLocation.Checksum = Int32.Parse(array[7]);

            return SatLocation;
        }
    }
}
