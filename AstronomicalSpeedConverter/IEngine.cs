﻿
namespace AstronomicalSpeedConverter
{
    public interface IEngine
    {
        public double Speed { get; }
    }
}
