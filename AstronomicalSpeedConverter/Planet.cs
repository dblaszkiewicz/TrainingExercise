﻿
namespace AstronomicalSpeedConverter
{
    public class Planet
    {
        public string Name { get; }
        public readonly double DistanceFromEarthInAU;

        public Planet(string name, double distance)
        {
            this.Name = name;
            this.DistanceFromEarthInAU = distance;
        }
    }
}
