﻿
namespace AstronomicalSpeedConverter
{
    public class FusionEngine : IEngine
    {
        private double _speed = 26000;
        public double Speed { get { return _speed; } }
    }
}
