﻿
namespace AstronomicalSpeedConverter
{
    public class Spacecraft
    {
        public IEngine Engine { get; set; }

        public Spacecraft(IEngine engine)
        {
            this.Engine = engine;
        }

    }
}
