﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AstronomicalSpeedConverter
{
    public class SpaceJourney
    {
        public Planet Destination { get; set; }

        public Spacecraft Spacecraft{ get; set; }

        public SpaceJourney(Spacecraft spacecraft, Planet destination)
        {
            this.Spacecraft = spacecraft;
            this.Destination = destination;
        }

        public double GetTravelTimeSeconds()
        {
            return Logic.GetTravelTimeSeconds(Destination.DistanceFromEarthInAU, Spacecraft.Engine.Speed);
        }

        public double GetTravelTimeDays()
        {
            var seconds = GetTravelTimeSeconds();
            return Logic.GetDaysFromSeconds(seconds);
        }

        public double GetTravelTimeHours()
        {
            var seconds = GetTravelTimeSeconds();
            return Logic.GetHoursFromSeconds(seconds);
        }
    }
}
