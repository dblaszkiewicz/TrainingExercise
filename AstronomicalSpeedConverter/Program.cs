﻿using System;

namespace AstronomicalSpeedConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            var starship = new Spacecraft(new ChemicalEngine());
            var destination = new Planet("Mars", 1.52);
            var journey = new SpaceJourney(starship, destination);
            var time = journey.GetTravelTimeDays();
            Console.WriteLine($"Podroz na Marsa statkiem z napędem chemicznym potrwa: {time} dni");

            starship.Engine = new NuclearEngine();
            time = journey.GetTravelTimeHours();
            Console.WriteLine($"Podroz na Marsa statkiem z napędem jądrowym potrwa: {time} godzin");

            starship.Engine = new FusionEngine();
            time = journey.GetTravelTimeHours();
            Console.WriteLine($"Podroz na Marsa statkiem z napędem termojądrowym potrwa: {time} godzin");

            Console.ReadKey();
        }
    }
}
