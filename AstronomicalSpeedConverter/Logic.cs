﻿using System;

namespace AstronomicalSpeedConverter
{
    public class Logic
    {
        public static readonly double AU = 150000000;
        public static double ConvertToKM(double distanceInAu)
        {
            return distanceInAu * AU;
        }

        public static double GetTravelTimeSeconds(double distanceInAU, double speed)
        {
            var distanceInKM = ConvertToKM(distanceInAU);
            var timeInSeconds = distanceInKM / speed;
            return Math.Round(timeInSeconds, 2);
        }

        public static double GetDaysFromSeconds(double seconds)
        {
            var timeInHours = seconds / 60 / 60;
            if(timeInHours < 48)
            {
                return Math.Round(timeInHours, 3);
            }
            var timeInDays = timeInHours / 24;
            return Math.Round(timeInDays, 2);
        }

        public static double GetHoursFromSeconds(double seconds)
        {
            var timeInHours = seconds / 60 / 60;
            return Math.Round(timeInHours, 2);
        }
    }
}
