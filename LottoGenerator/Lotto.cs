﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LottoGenerator
{
    public class Lotto
    {
        public static IEnumerable<int> GenerateNumbers()
        {
            var resultList = new List<int>();
            var random = new Random();

            while (resultList.Count != 6)
            {
                var randomNumber = random.Next(1, 49);
                if(resultList.Contains(randomNumber))
                {
                    continue;
                }
                resultList.Add(randomNumber);
            }

            return resultList.OrderBy(a => a);
        }

        public static void PrintNumbers(IEnumerable<int> Numbers)
        {
            Console.Write("Lotto results: ");
            foreach(var num in Numbers)
            {
                Console.Write($"{num} ");
            }
            Console.WriteLine();
        }
    }
}
