﻿using System;

namespace LottoGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("Press any key to start draw numbers \t 0 - exit program");
                if(Console.ReadKey().KeyChar == '0')
                {
                    return;
                }
                Console.Clear();
                Lotto.PrintNumbers(Lotto.GenerateNumbers());
            }
        }
    }
}
